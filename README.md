# routing

Show basic routing functionality with polymer/lit-element

## Test it out

- `npm install`
- `npm start`

## Explanation

The project is set up with a basic index.html. The `<base href="/">` tag in the header informs polymer that it should always fetch this html document for all subpaths. Without this tag we will get a 404 error when we refresh the web page on subpaths (e.g. /contests/3/edit).

The root element is called `my-app`. This is where the routing logic is implemented. 

### Install the router

The router we use is located in the package pwa-helpers. Check out [router.js](https://github.com/Polymer/pwa-helpers/blob/master/router.js). The code is merely 27 lines.

This is the routing logic we build on top of that:

```js
  _firstRendered() {
    installRouter((location) => { this.navigate(location )})
  }

  navigate(location) {
    const path = location.pathname === '/' ? 'home' : location.pathname.slice(1)
    let page = path.split('/')[0]

    if (['contests', 'view3', 'view4', 'about', 'home'].indexOf(page) === -1) {
      page = '404'
    }

    this.page = page
    this.path = path

    this._loadPage(page)
  }
```

The root gets resolved to page `home`. Any page that is not declared resolves to `404`. We store page and path as properties. Thus we have to declare them.

```js
  static get properties() {
    return {
      page: String,
      path: String
    }
  }
```

### Lazy loading

We could simply import all of our page elements at the top of the file. On initial page load all the elements would be loaded. To load pages lazily we use an asynchrounous import function. Look at the Network tab in your browsers dev tools to see which js-files are fetched when.

```js
  async _loadPage(page) {
    switch(page) {
      case 'contests':
        await import('./view-contests.js')
        break;
      case 'view3':
        await import('./view-3.js')
        break;
      case 'about':
        await import('./view-about.js')
        break;
      case 'home':
        await import('./view-home.js')
        break;
      case 'view  4':
        await import('./view-4.js')
        break;
      default:
        await import('./view-404.js')
    }
  }
```

### Conditional rendering

```html
<style>
    .page { display: none; }
    .page[active] { display: block; }
</style>
<main>
    <view-home class="page" active?="${page === 'home'}"></view-home>
    <view-404 class="page" active?="${page === '404'}"></view-404>
    <view-3 class="page" active?="${page === 'view3'}"></view-3>
    <view-4 class="page" active?="${page === 'view4'}"></view-4>
    <view-about class="page" active?="${page === 'about'}"></view-about>
    <view-contests class="page" active?="${page === 'contests'}"></view-contests>
</main>
```

If a view gets displayed is based on an active attribute. However its javascript can still run. All the other elements are basic Elements. Look at `view-home.js` to see how the rendering is prevented when the element is not displayed:

```js
  _shouldRender(props, changedProps, old) {
    return props.active;
  }

  static get properties() {
    return {
      active: Boolean
    }
  }
```
