import { LitElement, html } from '@polymer/lit-element'
import { installRouter } from 'pwa-helpers/router'

class MyApp extends LitElement {
  _render({ path, page }) {
    return html`
<a href="/"><h1>My App!</h1></a>
<hr>
<p>Path: ${path}</p>
<p>Page: ${page}</p>
<hr>
<a href="/contests">Contests</a>
<a href="/about">About</a>
<a href="/view3">New view</a>
<a href="/view4">anchor</a>
<a href="/contests/1">C1</a>
<a href="/contests/2">C2</a>
<a href="/contests/3">C3</a>
<a href="/asdkjfhksdfh">Not Found</a>
<hr>
<style>
    .page { display: none; }
    .page[active] { display: block; }
</style>
<main>
    <view-home class="page" active?="${page === 'home'}"></view-home>
    <view-404 class="page" active?="${page === '404'}"></view-404>
    <view-3 class="page" active?="${page === 'view3'}"></view-3>
    <view-4 class="page" active?="${page === 'view4'}"></view-4>
    <view-about class="page" active?="${page === 'about'}"></view-about>
    <view-contests class="page" active?="${page === 'contests'}"></view-contests>
</main>
<hr>
<p> Made with <3 by Alfred Melch</p>
`
    _
  }

  _firstRendered() {
    installRouter((location) => { this.navigate(location) })
  }

  navigate(location) {
    const path = location.pathname === '/' ? 'home' : location.pathname.slice(1)
    let page = path.split('/')[0]

    if (['contests', 'view3', 'view4', 'about', 'home'].indexOf(page) === -1) {
      page = '404'
    }

    this.page = page
    this.path = path

    this._loadPage(page)
  }

  static get properties() {
    return {
      page: String,
      path: String
    }
  }

  async _loadPage(page) {
    switch(page) {
      case 'contests':
        await import('./view-contests.js')
        break;
      case 'view3':
        await import('./view-3.js')
        break;
      case 'about':
        await import('./view-about.js')
        break;
      case 'home':
        await import('./view-home.js')
        break;
      case 'view  4':
        await import('./view-4.js')
        break;
      default:
        await import('./view-404.js')
    }
  }
}

window.customElements.define('my-app', MyApp)
