import { LitElement, html } from '@polymer/lit-element'

class View3 extends LitElement {
  _render() { return html`<h2>View 3</h2>` }
}

window.customElements.define('view-3', View3);
