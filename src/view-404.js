import { LitElement, html } from '@polymer/lit-element'

class View404 extends LitElement {
  _render() { return html`<h2>404 Page not found</h2>` }
}

window.customElements.define('view-404', View404);
