import { LitElement, html } from '@polymer/lit-element'

class ViewAbout extends LitElement {
  _render() {
    return html`
<h2>About</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam cum, dicta distinctio est et expedita magni minima nihil non pariatur quibusdam sapiente sint? At aut nihil reprehenderit voluptas voluptate!</p>
`
  }
}

window.customElements.define('view-about', ViewAbout);
