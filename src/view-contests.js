import { LitElement, html } from '@polymer/lit-element'

class ViewContests extends LitElement {
  _render() { return html`<h2>Contests</h2>` }
}

window.customElements.define('view-contests', ViewContests);
