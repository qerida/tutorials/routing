import { LitElement, html } from '@polymer/lit-element'

class ViewHome extends LitElement {
  _shouldRender(props, changedProps, old) {
    return props.active;
  }

  static get properties() {
    return {
      active: Boolean
    }
  }

  _render({path, page}) {
    return html`
<h2>Homepage</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ad corporis distinctio dolor harum libero minima nisi placeat similique, temporibus? Atque eligendi modi quibusdam quisquam repellat! Atque blanditiis dolore dolores!</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ad corporis distinctio dolor harum libero minima nisi placeat similique, temporibus? Atque eligendi modi quibusdam quisquam repellat! Atque blanditiis dolore dolores!</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ad corporis distinctio dolor harum libero minima nisi placeat similique, temporibus? Atque eligendi modi quibusdam quisquam repellat! Atque blanditiis dolore dolores!</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ad corporis distinctio dolor harum libero minima nisi placeat similique, temporibus? Atque eligendi modi quibusdam quisquam repellat! Atque blanditiis dolore dolores!</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ad corporis distinctio dolor harum libero minima nisi placeat similique, temporibus? Atque eligendi modi quibusdam quisquam repellat! Atque blanditiis dolore dolores!</p>
`
  }
}

window.customElements.define('view-home', ViewHome);
